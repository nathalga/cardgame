package no.ntnu.idatt2003.demo;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * The type Card game.
 */
public class CardGame extends Application {

  /**
   * The Deck of cards.
   */
  DeckOfCards deckOfCards = new DeckOfCards();

  /**
   * The Hand of cards.
   */
  HandOfCards handOfCards = new HandOfCards(DeckOfCards.dealHand(5));

  /**
   * The Deal hand.
   */
  Button dealHand = new Button("Deal hand");

  /**
   * The Check hand.
   */
  Button checkHand = new Button("Check hand");

  /**
   * The Shuffle deck.
   */
  Button shuffleDeck = new Button("Shuffle deck");

  /**
   * The Sum.
   */
  Label sum = new Label("Sum of hand: " + handOfCards.getSumOfHand());

  /**
   * The Check flush.
   */
  Label checkFlush = new Label("Flush: " + handOfCards.checkFlush());

  /**
   * The Check hearts.
   */
  Label checkHearts = new Label("Hearts: " + handOfCards.checkHearts());

  /**
   * The Check queen of spades.
   */
  Label checkQueenOfSpades = new Label("Queen of spades: " + handOfCards.checkQueenOfSpades());


  /**
   * The entry point of application for java fx.
   *
   * @param args the input arguments
   */
  public static void main(String[] args) {
    launch(args);
  }

  /**
   * Start method for java fx.
   * Sets the font and size of the labels and buttons.
   * Sets the action for the buttons.
   *
   * @param primaryStage the primary stage
   */
  @Override
  public void start(Stage primaryStage) {

    sum.setFont(Font.font("Times New Roman", 20));
    checkFlush.setFont(Font.font("Times New Roman", 20));
    checkHearts.setFont(Font.font("Times New Roman", 20));
    checkQueenOfSpades.setFont(Font.font("Times New Roman", 20));

    BorderPane pane = new BorderPane();
    VBox results = new VBox();
    results.getChildren().addAll(sum, checkFlush, checkHearts, checkQueenOfSpades);
    results.setVisible(false);
    VBox hand = new VBox();
    hand.getChildren().addAll(dealHand, checkHand, shuffleDeck);
    HBox cards = new HBox();
    VBox vBox = new VBox();
    vBox.getChildren().addAll(hand, results, cards);
    vBox.setAlignment(Pos.CENTER);
    HBox hBox = new HBox();
    hBox.getChildren().add(vBox);
    hBox.setAlignment(Pos.CENTER);
    pane.setCenter(hBox);

    dealHand.setOnAction(e -> {
          cards.getChildren().clear();
          handOfCards.playerHand(5);
          cards.getChildren().add(fillHand());
          results.getChildren().clear();
    });

    checkHand.setOnAction(e -> {
      results.setVisible(true);
      results.getChildren().add(results());
    });

    shuffleDeck.setOnAction(e -> {
      deckOfCards.fillDeck();
      dealHand.setDisable(false);
      shuffleDeck.setDisable(true);
    });

    Scene scene = new Scene(pane);
    primaryStage.setScene(scene);
    primaryStage.setMaximized(true);
    primaryStage.setTitle("CardGame");
    primaryStage.show();
  }

  /**
   * Results v box.
   * Sets the text for the labels in the results v box.
   *
   * @return the v box
   */
  public VBox results() {
    VBox vBox = new VBox();
    sum.setText("Sum of cards : " + handOfCards.getSumOfHand());
    checkFlush.setText("Flush : " + handOfCards.checkFlush());
    if (handOfCards.checkHearts().isEmpty()) {
      checkHearts.setText("There are no hearts in your hand");
      System.out.println("There are no hearts in your hand");
    } else  {
      checkHearts.setText("Check hearts : " + handOfCards.checkHearts().toString());
    }
    checkQueenOfSpades.setText("Check Queen of Spades : " + handOfCards.checkQueenOfSpades());
    vBox.getChildren().addAll(sum, checkFlush, checkHearts, checkQueenOfSpades);
    vBox.setAlignment(Pos.CENTER_LEFT);
    vBox.setPadding(new Insets(45));
    vBox.setStyle("-fx-font: 10px Verdana;");
    return vBox;
  }

  /**
   * Fill hand h box.
   * Fills the hand of cards with the cards from the hand of cards array list.
   * Sets the font and color of the cards.
   *
   * @return the h box
   */
  public HBox fillHand(){
    HBox cards = new HBox();
    cards.setAlignment(Pos.CENTER);
    cards.setSpacing(10);
    for (PlayingCard card : handOfCards.getHand()) {
      Label cardLabel = new Label(card.getAsString());
      cardLabel.setFont(Font.font("Times New Roman", 28));
      if (card.getSuit() == 'H' || card.getSuit() == 'D'){
        cardLabel.setStyle("-fx-text-fill: red");
      } else {
        cardLabel.setStyle("-fx-text-fill: black");
      }
      cards.getChildren().add(cardLabel);
    }
    return cards;
  }
}
