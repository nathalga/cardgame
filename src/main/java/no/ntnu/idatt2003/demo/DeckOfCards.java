package no.ntnu.idatt2003.demo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/**
 * Represents a Deck of cards, in an arraylist containing playing cards.
 */
public class DeckOfCards {
  static Random rand = new Random();
  private static final ArrayList<PlayingCard> deckOfCards = new ArrayList<>();

  /**
   * Instantiates a new Deck of cards.
   */
  public DeckOfCards() {
    fillDeck();
  }

  /**
   * Fills deck array list, with playing card objects. The method uses a two-dimensional loop to
   * through every possible combinations of playing cards and adds them to the deck of cards.
   * The method uses Collections shuffle to make sure the order of the playing cards are random.
   *
   * @return deckOfCards (array list)
   */
  public ArrayList<PlayingCard> fillDeck() {
    if (!deckOfCards.isEmpty()) {
      deckOfCards.clear();
    }
    char[] suits = { 'S', 'H', 'D', 'C' };
    for (char suit : suits) {
      for (int value = 1; value <= 13; value++) {
        deckOfCards.add(new PlayingCard(suit, value));
      }
    }
    Collections.shuffle(deckOfCards);
    return deckOfCards;
  }

  /**
   * Deals a hand of cards, by removing a random card from the deck of cards and adding it to the
   * hand of cards. The method uses a for loop to add a random card from the deck of cards to the
   * hand of cards, and then removes the card from the deck of cards.
   *
   * @param n the number of cards to deal
   * @return the hand of cards
   */
  public static ArrayList<PlayingCard> dealHand(int n) {
    if (n < 1 || n > 52) {
      throw new IllegalArgumentException("The number of cards must be between 1 and 52");
    }
    ArrayList<PlayingCard> hand = new ArrayList<>();

    for (int i = 0; i < n; i++) {
      PlayingCard card =  deckOfCards.get(rand.nextInt(deckOfCards.size()));
      hand.add(new PlayingCard(card.getSuit(), card.getFace()));
      deckOfCards.remove(card);
    }
    return hand;
  }

  /**
   * Gets deck of cards.
   *
   * @return the deck of cards
   */
  public ArrayList<PlayingCard> getDeckOfCards() {
    return deckOfCards;
  }
}
