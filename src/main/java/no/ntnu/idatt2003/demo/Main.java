package no.ntnu.idatt2003.demo;

/**
 * The type Main.
 */
public class Main {

  /**
   * The entry point of application.
   *
   * @param args the input arguments
   */
  public static void main(String[] args) {
    CardGame.main(args);
  }
}