package no.ntnu.idatt2003.demo;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Tests the Deck of cards class.
 */
class DeckOfCardsTest {
  private DeckOfCards deck;

  /**
   * Sets up.
   * Instantiates a new Deck of cards.
   */
  @BeforeEach
  public void setUp() {
    deck = new DeckOfCards();
  }

  /**
   * Fill deck test.
   * Tests if the deck is filled with 52 cards.
   */
  @Test
  public void fillDeckTest() {
    ArrayList<PlayingCard> filledDeck = deck.fillDeck();
    assertEquals(52, filledDeck.size(), "Deck should have 52 cards after filling.");
  }

  /**
   * Deal hand test.
   * Tests if the hand is dealt with 5 cards and if the cards in the hand are unique.
   */
  @Test
  public void dealHandTest() {
    ArrayList<PlayingCard> hand = DeckOfCards.dealHand(5);
    assertEquals(5, hand.size(), "Hand should have 5 cards after dealing.");

    // Ensure cards in hand are unique
    for (int i = 0; i < hand.size(); i++) {
      for (int j = i + 1; j < hand.size(); j++) {
        assertNotEquals(hand.get(i), hand.get(j), "Cards in hand should be unique.");
      }
    }
  }

  /**
   * Get deck of cards test.
   * Tests if the deck is filled with 52 cards and if the deck contains the Ace of Spades, King of
   * Hearts, 7 of Diamonds and 10 of Clubs.
   */
  @Test
  public void getDeckOfCardsTest() {
    ArrayList<PlayingCard> deckOfCards = deck.getDeckOfCards();
    assertEquals(52, deckOfCards.size(), "Deck should have 52 cards.");
    assertTrue(deckOfCards.contains(new PlayingCard('S', 1)), "Deck should contain Ace of Spades.");
    assertTrue(deckOfCards.contains(new PlayingCard('H', 13)), "Deck should contain King of Hearts.");
    assertTrue(deckOfCards.contains(new PlayingCard('D', 7)), "Deck should contain 7 of Diamonds.");
    assertTrue(deckOfCards.contains(new PlayingCard('C', 10)), "Deck should contain 10 of Clubs.");
  }
}