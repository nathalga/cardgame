package no.ntnu.idatt2003.demo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests the Hand of cards class.
 */
public class HandOfCardsTest {

  private HandOfCards hand;

  /**
   * Sets up.
   * Instantiates a new Hand of cards.
   */
  @BeforeEach
  public void setUp() {
    ArrayList<PlayingCard> cards = new ArrayList<>();
    cards.add(new PlayingCard('H', 2));
    cards.add(new PlayingCard('C', 5));
    cards.add(new PlayingCard('D', 10));
    cards.add(new PlayingCard('S', 12));
    cards.add(new PlayingCard('H', 13));
    hand = new HandOfCards(cards);
  }

  /**
   * Player hand test.
   * Tests if the hand is dealt with 3 cards and if the cards in the hand are unique.
   */
  @Test
  public void playerHandTest() {
    hand.playerHand(3);
    assertEquals(3, hand.getHand().size(), "Hand size should be 3 after "
        + "playerHand method.");
  }

  /**
   * Get sum of hand test.
   * Tests if the sum of the hand, in this case 42
   */
  @Test
  public void getSumOfHandTest() {
    assertEquals(42, hand.getSumOfHand(), "Sum of hand should be 42.");
  }

  /**
   * Check flush test.
   * Tests if the hand is a flush.
   */
  @Test
  public void checkFlushTest() {
    assertTrue(hand.checkFlush(), "Hand should be a flush.");
  }

  /**
   * Check hearts test.
   * Tests if the hand contains 2 of Hearts and King of Hearts.
   */
  @Test
  public void checkHeartsTest() {
    ArrayList<String> hearts = hand.checkHearts();
    assertEquals(2, hearts.size(), "There should be 2 hearts in hand.");
    assertTrue(hearts.contains("H2"), "Hand should contain 2 of Hearts.");
    assertTrue(hearts.contains("H13"), "Hand should contain King of Hearts.");
  }

  /**
   * Check queen of spades test.
   * Tests if the hand contains Queen of Spades.
   */
  @Test
  public void checkQueenOfSpadesTest() {
    assertTrue(hand.checkQueenOfSpades(), "Hand should contain Queen of Spades.");
  }
}